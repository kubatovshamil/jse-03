package ru.t1.kubatov.tm.constant;

public final class TeminalConst {

    public static final String INFO = "info";

    public static final String VERSION = "version";

    public static final String HELP = "help";

}
